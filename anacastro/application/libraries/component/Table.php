<?php
//representa o componente table do html

class Table {
    private $data;
    private $label;
    
    function __construct(array $data, array $label){
        $this->data = $data;
        $this->label = $label;
        
        
    }

    private function header(){
        $html = '<thead><tr>';

        foreach($this->label as $col){
            $html .= '<th scope="col">'.$col.'</yh>';
        }
              
        $html .= '</tr></thead>';
        return $html;
    }
    private function body(){
        $html = '<tbody>'.
                $this->rows().
                '</tbody>';
        return $html;
    }
    private function rows(){
        $html = ''; $i = 0;
        
        foreach ($this->data as $row) {
            $html .= '<tr>';

            foreach($row as $col) {
                $html .= '<td>'. $col .'</td>';
            }
                
            $html .= '</tr>'; 
        }
        return $html;
    }

    public function getHTML() {
        $html = '
        <table class="table">'.//vai receber o codigo html e retornar a chamada
            $this->header().//o ponto vai concatenar
            $this->body().
        '</table>';

        return $html;
    }

}
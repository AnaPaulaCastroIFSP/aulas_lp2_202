<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//pagina carregada pela primeira vez(formulario vazio) ou quando salva informacao
class Contas extends MY_Controller {

    public function index(){
            echo 'Lista de todas as contas';
    }
    //controlador contas a pagar
    public function pagar($mes = 0, $ano = 0){// 0 para indicar o mes e ano atual
        // salva os dados da nova conta caso exista
        $this->load->model('ContasModel', 'conta');//conta e apelido
        $this->conta->cria('pagar');

        // recupera a lista de contas a pagar
        $v['lista'] = $this->conta->lista('pagar');
        $v['tipo']= 'pagar';


        // carrega a view e passa a lista de contas
        $html = $this->load->view('contas/lista_contas', $v, true);
        
        // exibe esta view
        $this->show($html);
    }
    //controlador contas a receber
    public function receber($mes = 0, $ano = 0){// 0 para indicar o mes e ano atual

    }
}
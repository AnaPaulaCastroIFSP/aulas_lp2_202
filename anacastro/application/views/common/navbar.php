<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <!-- Container wrapper -->
    <div class="container-fluid">
        <!-- Toggle button -->
        <a class="navbar-brand" href="#">Controle Financeiro</a>
        <button
        class="navbar-toggler"
        type="button"
        data-mdb-toggle="collapse"
        data-mdb-target="#navbarLeftAlignExample"
        aria-controls="navbarLeftAlignExample"
        aria-expanded="false"
        aria-label="Toggle navigation"
        >
        <i class="fas fa-bars"></i>
        </button>

        <!-- Collapsible wrapper -->
        <div class="collapse navbar-collapse" id="navbarLeftAlignExample">
        <!-- Left links -->
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item active">
                <a class="nav-link active" aria-current="page" href="<?= base_url('home')?>">Home</a>
            </li>

            <!-- Navbar dropdown -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" 
                role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Cadastro
                </a>
            <!-- Dropdown menu -->
                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <li>
                        <a class="dropdown-item" href="<?= base_url('usuario/cadastro')?>">Usuário</a>
                    </li>
                    <li>
                        <a class="dropdown-item" href="#">Conta Bancaria</a>
                    </li>
                    <li>
                        <a class="dropdown-item" href="#">Parceiros</a>
                    </li>
                </ul>
            </li>

             <!-- Navbar dropdown -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" 
                role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Lançamentos
                </a>
            <!-- Dropdown menu -->
                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <li>
                        <a class="dropdown-item" href="#">Contas a Pagar</a>
                    </li>
                    <li>
                        <a class="dropdown-item" href="#">Contas a Receber</a>
                    </li>
                    <li>
                        <a class="dropdown-item" href="#">Fluxo de caixa</a>
                    </li>
                </ul>
            </li>

            <!-- Navbar dropdown -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" 
                role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Relatórios
                </a>
            <!-- Dropdown menu -->
                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <li>
                        <a class="dropdown-item" href="#">Lançamento por Periodo</a>
                    </li>
                    <li>
                        <a class="dropdown-item" href="#">Resumo Mensal</a>
                    </li>
                    <li>
                        <a class="dropdown-item" href="#">Resumo Anual</a>
                    </li>
                </ul>
            </li>
            
        </ul>
        <!-- Left links -->
        </div>
        <!-- Collapsible wrapper -->
    </div>
  <!-- Container wrapper -->
</nav>
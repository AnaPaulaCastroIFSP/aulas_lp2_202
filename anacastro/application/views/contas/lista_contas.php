<div class="container">

    <div class = "mt-5">
        <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
            Nova Conta
        </a>
    </div>

    <div class="collapse" id="collapseExample">
        <div class="row">
            <div class="col-md-6 mx-auto border mt-5 pt-3 pb-3">
                <form method="POST" id="contas-form">

                    <input type="text" name="parceiro"  class="form-control" placeholder="Devedor / Credor"/><br>
                    <input type="text" name="descricao" class="form-control" placeholder="Descrição"/><br>
                    <input type="number" name="valor"  class="form-control" placeholder="Valor" /><br><br>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <input type="number" name="mes"  class="form-control" placeholder="Mês" />
                        </div>
                        <div class="col-md-6">
                        <input type="number" name="ano"  class="form-control" placeholder="Ano" />
                        </div>
                    </div>
                    
                    <input type="hidden" name="tipo" value="<?= $tipo?>"><br>

                    <div class="text-center text-md-left">
                        <a class="btn btn-primary" onclick="document.getElementById('contas-form').submit();">Enviar</a>
                    </div>

                </form>

            
            </div>
        </div>
    </div>
   

    <div class="row mt-5">
        <div class="col">
            <?= $lista?>
        </div>
    </div>

</div>
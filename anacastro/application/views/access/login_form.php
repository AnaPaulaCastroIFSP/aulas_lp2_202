<div style="height: 100vh">
    <div class="flex-center flex-column">

        <h3 class="mb-5">Controle Financeiro Pessoal</h3>
        
        <form method="POST">

            <p class="h4 mb-4">Entrar</p>
            <div class="form-outline mb-4">
                <input type="email" id="email" name="email" class="form-control" />
                <label class="form-label" for="email">E-mail</label>
            </div>

            <div class="form-outline mb-4">
                <input type="password" id="senha" name="senha" class="form-control" />
                <label class="form-label" for="senha">Senha</label>
            </div>
         
            <button type="submit" class="btn btn-primary btn-block mb-4">Enviar</button>

            <p class="red-text"><?= $error ? 'Dados de acesso incorretos.' : ''?></p>

        </form>

    </div>
</div>